import React, {Component} from 'react';
import { ThemeProvider, createTheme, Arwes} from 'arwes';
import classes from './App.module.css';
import BoxLayout from './Containers/Layout/BoxLayout';
import ReactDOM from 'react-dom';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import Login from './Containers/Login/Login';
import SignUp from './Containers/SignUp/SignUp';
import ResetPassword from './Containers/ResetPassword/ResetPassword';
import NewPassword from './Containers/NewPassword/NewPassword';
import GamePrepare from './Containers/GamePrepare/GamePrepare';
import HallOfFame from './Containers/HallOfFame/HallOfFame';
import Rules from './Containers/Rules/Rules';
import image from './images/background.jpg';
import GameRouter from './Containers/GameRouter/GameRouter';
import { PrivateRoute } from './Component/PrivateRouter/PrivateRouter';

const myTheme = {
  color: {
    primary: {
      base: '#74c1da',
      dark: '#468ca2',
      light: '#9cd8eb'
    },
    header: {
      base: '#74c1da',
      dark: '#468ca2',
      light: '#9cd8eb'
    }
  }
};


class App extends Component {
  render(){
    return (
      <ThemeProvider theme={createTheme(myTheme)}>
        <Arwes animate background={image} >
          <BrowserRouter>
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/signup" component={SignUp} />
              <Route path="/resetpassword" component={ResetPassword} />
              <Route path="/newpassword" component={NewPassword} />
              <Route path="/rules" component={Rules} />
              <PrivateRoute path="/game" component={GameRouter} isLogged />
              <Route path="/halloffame" component={HallOfFame} />
              <Route path="*"> <Redirect to='/login'/> </Route>
            </Switch>
          </BrowserRouter>
        </Arwes>
      </ThemeProvider>
    );
 }
}

export default React.memo(App);

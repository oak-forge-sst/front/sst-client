import React, {Component} from "react";
import "./MainMenu.css"
import FrameFromArwes from '../../Component/FrameFromArwes/FrameFromArwes.js'
import PropTypes from 'prop-types';


class MainMenu extends Component {
    render(){
        return(
            <FrameFromArwes>
             <div className="MainMenu-wrapper">
                <div className="MainMenu-wrapper-text">
                  <button onClick={this.props.onClose}>
                      Close
                  </button>
                  <ul>
                      <li>NEW GAME</li>
                      <li>LOG OUT</li>
                  </ul>
                </div>
              </div>
            </ FrameFromArwes >


        )
    }
}

MainMenu.propTypes = {
  onClose: PropTypes.func.isRequired,
}

export default MainMenu;

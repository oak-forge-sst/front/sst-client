import React, { Component } from 'react'
import './CommandPrompt.css';


const commands = ["command 1", "command 2", "command 3", "command 4", "command 5", "command 6", "command 7", "command 8", "command 9", "command 10", ]



class CommandPrompt extends Component {
    constructor(props){
        super(props);
        this.state = {
            showCommands: false,
            command: ""
        }
    this.toggleCommands = this.toggleCommands.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleCommands(){
        this.setState({showCommands: !this.state.showCommands})
    }
    
    handleChange(event){
        this.setState({[event.target.name]: event.target.value})
    }

    handleSubmit(event){
        const newValue = (event.target.value).toLowerCase()
        if (event.key === 'Enter' && commands.includes(newValue)) {
         event.preventDefault();
         event.stopPropagation();
         alert(`You entered ${event.target.value}`);
        this.setState({command: ""});
    } else if (event.key === 'Enter') {
        alert("COMMAND NOT FOUND!")
        this.setState({command: ""});
    }
}
    render(){
        return(
            <div className="CommandPrompt">
            <div className="Commands">
                {(this.state.showCommands) &&
                <div>
                    <button onClick={this.toggleCommands}>Close</button>
                    <ul>
                    {commands.map(command => 
                    <li>{command}</li>
                    )}
                    </ul>
                </div>

                } 
            </div>
            <div className="Prompt">
                <input 
                 type="text"
                 name="command"
                 value={this.state.command}
                 onChange={this.handleChange}
                 onKeyDown={this.handleSubmit}
                  />
                <button onClick={this.toggleCommands}>COMMANDS</button>
            </div>  
            </div>

        )
    }
}
export default CommandPrompt;
import React, {Component} from 'react';
import ArwesInput from "../../Component/ArwesInput/ArwesInput";
import ButtonToForm from "../../Component/ButtonToForm/ButtonToForm";
import classes from "./NewPassword.module.css";
import {validation} from "../../Component/ValidationAndMessage/Validation";


class NewPassword extends Component {
     constructor(props){
          super(props);
          this.state = {
               password: "",
               confirmPassword: "",
               touched: false
          }
          this.handleChange = this.handleChange.bind(this);
          this.handleClick = this.handleClick.bind(this);
     }

     handleChange(evt){
       if(validation(evt)){
         this.setState({
           [evt.target.name]: [evt.target.value]
         })
       }else{
         this.setState({
           [evt.target.name]: ''
         })
       }
     }


     handleClick(evt){
       evt.preventDefault()
       this.setState({
         touched: true
       })
     }


render(){
  return(
    <div>
     <h1>Reset Password</h1>
     <form
      className={classes.ResetForm}>
       <ArwesInput
            required
            type={"password"}
            placeholder={"New password"}
            name={"password"}
            onChange={this.handleChange}
            valid={this.state.touched && !this.state.password}
       />
       <ArwesInput
            required
            type={"password"}
            placeholder={"Repeat password"}
            name={"confirmPassword"}
            onChange={this.handleChange}
            valid={this.state.touched && !this.state.confirmPassword}
       />
       <div>
         {this.state.touched && (this.state.password[0] !== this.state.confirmPassword[0]) && "Passwords don't match!"}
       </div>

       <ButtonToForm
       animate
       onClick={this.handleClick}
       >
          Reset password
     </ButtonToForm>

     </form>
     </div>
   )
     }
};

export default NewPassword;

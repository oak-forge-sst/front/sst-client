import React, {Component} from 'react';
import {Link} from "react-router-dom";
import ArwesInput from "../../Component/ArwesInput/ArwesInput";
import classes from "./Login.module.css";
import ButtonToForm from "../../Component/ButtonToForm/ButtonToForm";
import {validation} from "../../Component/ValidationAndMessage/Validation";

class Login extends Component{
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      touched: false
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


 handleChange(evt){
   if(validation(evt)){
     this.setState({
       [evt.target.name]: [evt.target.value]
     })
   }else{
     this.setState({
       [evt.target.name]: ''
     })
   }
 }

  handleClick(evt){
     evt.preventDefault()
     this.setState({
       touched: true
     })
  }

  render(){
  return(
    <div>
     <h1>WELCOME TO SUPER STAR TREK!</h1>
     <form className={classes.LoginForm}>
       <ArwesInput
       required
       type={"email"}
       placeholder={"email"}
       name={"email"}
       onChange={this.handleChange}
       />

       <ArwesInput
       required
       type={"password"}
       placeholder={"password"}
       name={"password"}
       onChange={this.handleChange}
       />

       <ButtonToForm
       animate
       onClick={this.handleClick}
       >
        Log In
       </ButtonToForm>
       <div className={classes.SignUpWrapper}>
         <Link className={classes.ResetPassword} to="/resetpassword">Forgot password?</Link>
       </div>
       <div className={classes.SignUpWrapper}>
         <Link className={classes.SignUp} to="/signup">Sign Up</Link>
       </div>
     </form>
     </div>
  )
  }
};

export default Login;

import React from 'react';
import FrameFromArwes from '../../Component/FrameFromArwes/FrameFromArwes.js'
import classes from './Status.module.css';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import updateStatus from "../../store/actions/Status.actions"


const Status = (props) => {
  return(
     <FrameFromArwes additionalClass={classes.status} scrolled>
        <strong> Status </strong>
        <p>Score: {props.score.score}</p>
        <p>Civilization: {props.civilization.civilization}</p>
        <p>Opponents: {props.opponents.opponents}</p>
      </FrameFromArwes>
  );
};

const mapStateToProps = (state) => {
  return {
    score: state.statusReducer.score,
    civilization: state.statusReducer.civilization,
    opponents: state.statusReducer.opponents
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
     updateStatus : (state) => {
       dispatch(updateStatus)
     }
  }
}

Status.propTypes = {
  civilization: PropTypes.string.isRequired,
  score: PropTypes.number.isRequired,
  opponents: PropTypes.number.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Status)

import React, {Component} from 'react'
import ArwesInput from "../../Component/ArwesInput/ArwesInput";
import classes from "./ResetPassword.module.css";
import ButtonToForm from "../../Component/ButtonToForm/ButtonToForm";
import {validation} from "../../Component/ValidationAndMessage/Validation";

class ResetPassword extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            emailSent: false,
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(evt){
      if(validation(evt)){
        this.setState({
          [evt.target.name]: [evt.target.value]
        })
      }else{
        this.setState({
          [evt.target.name]: ''
        })
      }
    }

    handleClick(evt){
      this.setState({
        touched: true
      })

      if(this.state.email){
        evt.preventDefault()
        this.setState({
          emailSent: true
        })
      }
    }


    render(){
    return (
        <div>
        <form
         className={classes.ResetForm}>
           <h3>Write your e-mail</h3>
           <ArwesInput
            required
            type={"email"}
            placeholder={"email"}
            name={"email"}
            onChange={this.handleChange}
            valid={this.state.touched && !this.state.email}
           />

           <ButtonToForm
           animate
           onClick={this.handleClick}
           >Reset Password
           </ButtonToForm>

           <p className={classes.messageEmailSent}>{this.state.emailSent && "Your email address has been verified successfully. You can now reset the password for your account"}</p>
          </form>
        </div>
    )
  }
}
export default ResetPassword;

import React, {useState, useEffect} from 'react';
import FrameFromArwes from '../../Component/FrameFromArwes/FrameFromArwes.js'
import classes from './ActionBox.module.css';
import {connect} from 'react-redux';
import * as actionCreators from '../../store/actions/ActionBox.action';
import PropTypes from 'prop-types';


const Actionbox = ({pickedUnit}) => {

  const [image, setImage] = useState();

  useEffect(() => {
    if(pickedUnit){
      import(`../../images/images-point/${pickedUnit.type}.jpg`).then(
        img => setImage(img.default)
      );
    }
  }, [pickedUnit]);


  String.prototype.capitalize = function() {
      return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase().replace(/\_/g," ");
  }

  let pointType = null;

  if(pickedUnit){
    pointType = pickedUnit.type.capitalize();
  }


  return (
  <FrameFromArwes additionalClass={classes.actionBox} scrolled>
      <div className={classes.actionBoxWrapper}>
        <div className={classes.actionBoxWrapperImg}>
          <img src={image}/>
        </div>
          {pickedUnit && <div className={classes.actionBoxText}>
            <span> {pickedUnit.unitName} </span>
            <p> {pointType} </p>
            <p> Position: {pickedUnit.x} {pickedUnit.y} </p>
            {pickedUnit.health && <p> Health: {pickedUnit.health} </p>}
            {pickedUnit.actionPoints && <p> Points: {pickedUnit.actionPoints} </p>}
         </div>}
      </div>
  </ FrameFromArwes >
  )
}


const mapStateToProps = state => {
  return {
    pickedUnit: state.actionBoxReducer.selectedPoint
  };
};

Actionbox.propTypes = {
  pickedUnit: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(Actionbox);

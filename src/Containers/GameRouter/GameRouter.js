import React from 'react';
import {Link, Switch, Route, useRouteMatch} from 'react-router-dom';
import GamePrepare from '../GamePrepare/GamePrepare';
import BoxLayout from '../Layout/BoxLayout';


const Game = () =>{
  const {path} = useRouteMatch();

  return(
    <Switch>
      <Route exact path={`${path}/`} component={BoxLayout} />
      <Route path={`${path}/prepare`} component={GamePrepare} />
    </Switch>
  )
};

export default Game;

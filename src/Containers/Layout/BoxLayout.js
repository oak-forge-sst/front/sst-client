import React, { Component } from 'react';
import classes from './BoxLayout.module.css';
import MiniMap from '../MiniMap/MiniMap'
import ActionBox from '../ActionBox/ActionBox'
import Status from '../Status/Status'
import Units from '../Units/Units'
import MainMap from '../../Component/MainMap/MainMap'
import CommandPrompt from '../CommandPrompt/CommandPrompt'
import MainMenu from '../MainMenu/MainMenu';
import {Link} from 'react-router-dom';

class BoxLayout extends Component {

  constructor(props){
    super(props);
    this.state = { showMainMenu: false, typeOfChildren: "", selectedPointIndex: "", canvasParameters: {}}
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.closeMainMenu = this.closeMainMenu.bind(this);
    this.getPointFromChildrenComponent = this.getPointFromChildrenComponent.bind(this);
    this.getCanvasParameters = this.getCanvasParameters.bind(this);
  }

  getPointFromChildrenComponent(index, name){
    this.setState({selectedPointIndex: index, typeOfChildren: name})
  }

  handleKeyPress(event){
    if(event.key === 'Escape'){
     this.setState({showMainMenu: !this.state.showMainMenu})
    }
  }

  closeMainMenu(){
    this.setState({showMainMenu: false})
  }

  componentDidMount(){
    document.addEventListener("keydown", this.handleKeyPress, false);
  }
  componentWillUnmount(){
    document.removeEventListener("keydown", this.handleKeyPress, false);
  }

  getCanvasParameters(ref){
    let rect = ref.getBoundingClientRect();
    this.setState({
      canvasParameters: rect
    })
  }


  render(){
    return (
      <>
        {(this.state.showMainMenu) &&
            <MainMenu onClose={this.closeMainMenu}/>
        }
        <MainMap
          getPointIdFromMainMap={this.getPointFromChildrenComponent}
          getCanvasParameters={this.getCanvasParameters}
        />
        <div className={classes.horizontalBoxes}>
            <MiniMap
              canvasParameters={this.state.canvasParameters}
            />
            <ActionBox/>
            <CommandPrompt/>
        </div>
        <div className={classes.verticalBoxes}>
            <Status />
            <Units/>
        </div>
      </>
    );
 }
}


export default BoxLayout;

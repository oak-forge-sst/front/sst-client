import React, { Component } from 'react';
import FrameFromArwes from '../../Component/FrameFromArwes/FrameFromArwes.js';
import OneUnit from './OneUnit.js';
import classes from './Units.module.css';
import {selectpoint} from '../../store/actions/ActionBox.action'
import {connect} from 'react-redux';

class Units extends Component {

constructor(props){
  super(props);
}

  render(){

   const dataUnit = this.props.units.map(unit => {
              return <OneUnit
              getUnitIdFromOneUnit = {this.props.selectPoint}
              key = {unit.unitID}
              id = {unit.unitID}
              unitName = {unit.unitName}
              type = {unit.type}
              x = {unit.x}
              y = {unit.y}
              health = {unit.health}
              actionPoints = {unit.actionPoints}
              />
           });

    return(
        < FrameFromArwes additionalClass={classes.units} scrolled>
          {dataUnit}
        </ FrameFromArwes >
    );
  };
};

const mapStateToProps = state => {
  return {
    units: state.unitsReducer.fleetStatusDTOS
  };
};

const mapDispatchToProps = dispatch => {
  return {
    selectPoint: (point) => dispatch(selectpoint(point))
  };

};

export default connect(mapStateToProps, mapDispatchToProps)(Units);

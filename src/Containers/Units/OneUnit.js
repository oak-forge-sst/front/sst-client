import React, { Component } from 'react';
import classes from './OneUnit.module.css';
import PropTypes from 'prop-types';

class OneUnit extends Component {

constructor(props){
  super(props);
  this.state = {
      image: undefined
  }
  this.handleClick = this.handleClick.bind(this);
}


    componentDidMount(){
      import(`../../images/images-point/${this.props.type}.jpg`).then(
        img => this.setState({image: img.default})
      );
    }

handleClick(){
 this.props.getUnitIdFromOneUnit(this.props);
}

  render(){
    return (
      <div onClick={this.handleClick} className={classes.unit}>
        <div className={classes.unitImage}>
          <img src={this.state.image}/>
        </div>
        <div className={classes.unitContent}>
          <strong> {this.props.unitName} </strong>
          <p> position: {this.props.x} {this.props.y} </p>
          <p> health: {this.props.health} </p>
          <p> action points: {this.props.actionPoints} </p>
       </div>
      </div>
    )
  };
};

OneUnit.propTypes = {
  id: PropTypes.number.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  health: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  unitName: PropTypes.string.isRequired,
  actionPoints: PropTypes.number.isRequired,
  getUnitIdFromOneUnit: PropTypes.func.isRequired
}


export default OneUnit;

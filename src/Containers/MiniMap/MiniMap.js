import React, {useRef, useEffect} from 'react';
import FrameFromArwes from '../../Component/FrameFromArwes/FrameFromArwes.js'
import classes from './MiniMap.module.css';
import {connect} from "react-redux";
import PropTypes from 'prop-types';


const config = {
  sizeOfCell: 50, //in pixels
  sizeOfKwadrant: 6, //in cells
  world: {
      width: 10, //in kwadrants
      height: 10 //in kwadrants
  }
};

let widthInMiniMap;
let heightInMiniMap;
let {sizeOfKwadrant} = config;
let {width, height} = config.world;


if(width <= height) {
  widthInMiniMap = (width * 300)/ height;
  heightInMiniMap = 300;
} else {
  heightInMiniMap = (height * 300)/ width;
  widthInMiniMap = 300;
}


const Minimap = (props) => {


  function drawObject(ctx) {
    props.largeScan.map((unit) => {
      let positionOnXAxis = (unit["x"] * widthInMiniMap) / (sizeOfKwadrant * width);
      let positionOnYAxis = (unit["y"] * heightInMiniMap) / (sizeOfKwadrant * height);
      let cellWidth = widthInMiniMap / (sizeOfKwadrant * width)
      let cellHeight = heightInMiniMap / (sizeOfKwadrant * height)

      if (unit.type === "STARBASE"){
        ctx.fillStyle = "blue"
      }
      else if(unit.type === "ENTERPRISE"){
        ctx.fillStyle = "green"
      }
      else if (unit.type === "COMMANDER" || "SUPER_COMMANDER" || "KLINGON"){
        ctx.fillStyle = "red"
      }
      ctx.fillRect(positionOnXAxis - cellWidth, positionOnYAxis - cellHeight, cellWidth, cellHeight);
      ctx.stroke();
      })
    }

    function drawBorder(ctx) {
      const widthInMainMap = (window.innerWidth-300);
      const heightInMainMap = (window.innerHeight-300);
      let widthBorder
      let heightBorder
      let xStart
      let yStart

      if(widthInMainMap >= props.canvasParameters.width) {
        widthBorder= widthInMiniMap
      }else{
        widthBorder = (widthInMainMap / props.canvasParameters.width) * widthInMiniMap
      }

      if(heightInMainMap >= props.canvasParameters.height) {
        heightBorder= heightInMiniMap
      }else{
        heightBorder = (heightInMainMap / props.canvasParameters.height) * heightInMiniMap
      }

      if(props.canvasParameters.x >= 0){
        xStart = 0
      }else{
        xStart = (Math.abs(props.canvasParameters.x) / props.canvasParameters.width) * widthInMiniMap
      }

      if(props.canvasParameters.y >= 0){
        yStart = 0
      }else{
        yStart = (Math.abs(props.canvasParameters.y) / props.canvasParameters.height) * heightInMiniMap
      }

      ctx.beginPath();
      ctx.rect(xStart, yStart, widthBorder, heightBorder);
      ctx.strokeStyle = '#fff';
      ctx.lineWidth = 1;
      ctx.stroke();
    }

const canvas = useRef();

  useEffect(() => {
    canvas.current.width = widthInMiniMap;
    canvas.current.height = heightInMiniMap;
    const ctx = canvas.current.getContext("2d");
    drawObject(ctx);
    drawBorder(ctx);
    }
  ,[props.canvasParameters])


return(
  <FrameFromArwes additionalClass={classes.miniMap}>
    <div className={classes.miniMapWrapper}>
     <canvas ref={canvas}/>
    </div>
  </ FrameFromArwes >
)
};

const mapStateToProps = (state) => {
  return {
    largeScan: state.mapReducer.shortRangeScanObjects
  }
}

Minimap.propTypes = {
  canvasParameters: PropTypes.exact({
    bottom: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    left: PropTypes.number.isRequired,
    right: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired
  }).isRequired
}

export default connect(mapStateToProps)(Minimap)

import React from 'react';
import classes from './Notifications.module.css';
import FrameFromArwes from '../FrameFromArwes/FrameFromArwes';


const notifications = [
  {
    time: '00:00',
    type: "success",
    message: "You have scored 100 points."
  },
  {
    time: '00:00',
    type: "fail",
    message:"Your spaceship has been demaged."
  },
  {
    time: '00:00',
    type: "message",
    message: "Hi. I'm your enemy."
  },
  {
    time: '00:00',
    type: "fail",
    message:"Your spaceship has been demaged."
  },
  {
    time: '00:00',
    type: "fail",
    message:"Your spaceship has been demaged."
  },
  {
  time: '00:00',
  type: "success",
  message: "You have scored 100 points."
  },
  {
  time: '00:00',
  type: "success",
  message: "You have scored 100 points."
  },
  {
  time: '00:00',
  type: "success",
  message: "You have scored 100 points."
  },
  {
  time: '00:00',
  type: "success",
  message: "You have scored 100 points."
  },
  {
  time: '00:00',
  type: "success",
  message: "You have scored 100 points."
  },
  {
  time: '00:00',
  type: "success",
  message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
},
{
time: '00:00',
type: "success",
message: "You have scored 100 points."
}
]

let notificationsAndMessages = notifications.map((notification, index) => {
 return <p key={index} className={classes[notification.type]}> {notification.message} </p>
});

const Notifications = () => {
  return(
    <div className={classes.notifications}>
        <FrameFromArwes additionalClass={classes.notificationsFrame} scrolled>
          {notificationsAndMessages}
        </FrameFromArwes >
    </div>
   );
  };

export default Notifications;

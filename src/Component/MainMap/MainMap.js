import React, { Component } from 'react';
import PinchZoomPan from "react-responsive-pinch-zoom-pan";
import classes from "./MainMap.module.css";
import EnemyTurn from "./EnemyTurn.js";
import Notifications from "./Notifications.js";
import {selectpoint} from '../../store/actions/ActionBox.action'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

const pointsFromCanvas = []

const config = {
    sizeOfCell: 50,
    sizeOfKwadrant: 6,
    world: {
        width: 10,
        height: 10
    }
};

function position(a) {
  return (a * 50) + 25
}

function applyStyles(x, y, ctx, color, width) {
  ctx.fillStyle = color;
  ctx.fill();
  ctx.strokeStyle = color;
  pushToArray(x, y, width)
}

function drawStar(ctx, x, y, color) {
  let width = 20;
  ctx.arc(x, y, width/2 , 0, 2 * Math.PI);
  applyStyles(x, y, ctx, color, width);
}

function drawUnit(ctx, x, y, color) {
  let width = 20;
  ctx.rect(x - (width/2), y - (width/2), width, width);
  applyStyles(x, y, ctx, color, width);
}

function pushToArray(x, y, width){
  pointsFromCanvas.push({x: x-(width/2), y: y-(width/2), width: width, height: width})
}


const cw = config.sizeOfCell * config.sizeOfKwadrant * config.world.width;
const ch = config.sizeOfCell * config.sizeOfKwadrant * config.world.height;

function drawBoard(ctx) {
     for (let x = 0; x <= cw; x += config.sizeOfCell) {
           ctx.beginPath();
           ctx.moveTo( x, 0);   //vertical
           ctx.lineTo( x, ch);   //vertical

           let y = x;
           ctx.moveTo(0, y);   //horizontal
           ctx.lineTo(cw, y);  //horizontal

           if(x % (config.sizeOfKwadrant * config.sizeOfCell) === 0){
                 ctx.setLineDash([10, 25]);
                 ctx.strokeStyle = '#aaa';
           }
           else{
                 ctx.setLineDash([3, 10]);
                 ctx.strokeStyle = '#666';
           }

           ctx.stroke();
     }
}


class MainMap extends Component {
    constructor (props) {
        super(props);
        this.canvasRef = React.createRef();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e, ref){
        let rect = ref.getBoundingClientRect();
        let zoom = rect.width/cw;
        let positionX = e.clientX;
        let positionY = e.clientY
        let mouseX = parseInt((positionX - rect.left)/zoom);
        let mouseY = parseInt((positionY - rect.top)/zoom);

        pointsFromCanvas.map((point, index) => {
          if((mouseX >= point.x && mouseX <= point.x + point.width) &&
              (mouseY >= point.y && mouseY <= point.y + point.height)){
               this.props.selectPoint(this.props.points[index]);
             }
        });
      this.props.getCanvasParameters(ref)
    }


     componentDidMount() {
       const ctx = this.canvasRef.current.getContext("2d");

       this.canvasRef.current.width = cw;
       this.canvasRef.current.height = ch;

       let json = this.props.points

       function drawObject(ctx) {
          json.map((j, index) => {
           let x = position(json[index].x);
           let y = position(json[index].y);
           ctx.beginPath();
           if(json[index].type === "STAR"){
            drawStar(ctx, x, y, "#FF0000");
           }else if(json[index].type === "KLINGON"){
             drawUnit(ctx, x, y, "#00FF00");
           }else if(json[index].type === "COMMANDER"){
              drawUnit(ctx, x, y, "#33d7ff");
           }else if(json[index].type === "SUPER_COMMANDER"){
               drawUnit(ctx, x, y, "#f6ff33");
           }else if(json[index].type === "BLACK_HOLE"){
             drawStar(ctx, x, y, "#0000FF");
           }else if(json[index].type === "PLANET"){
             drawStar(ctx, x, y, "#33ff3c");
           }else if(json[index].type === "ENTERPRISE"){
               drawUnit(ctx, x, y, "#ff6133");
           }else if(json[index].type === "STARBASE"){
             drawStar(ctx, x, y, "#ff33c1");
           }
         });
         ctx.stroke();
       }

          drawBoard(ctx);
          drawObject(ctx);

      this.props.getCanvasParameters(this.canvasRef.current);

      ['wheel','mousewheel', 'DOMMouseScroll'].forEach((e) => {
       document.addEventListener(e, (e) => this.props.getCanvasParameters( this.canvasRef.current));
      });
     }


    render () {
        return (
          <div className={classes.mainMap}>
            <PinchZoomPan doubleTapBehavior='zoom' position='center' initialScale={1} zoomButtons={false}>
            <canvas onClick={(e) => {this.handleClick(e, this.canvasRef.current)}}
            ref={this.canvasRef} />
            </PinchZoomPan>
            < EnemyTurn />
          </div>
        );
    }
};

const mapStateToProps = state => {
  return {
    points: state.mapReducer.shortRangeScanObjects
  };
};

const mapDispatchToProps = dispatch => {
  return {
    selectPoint: (point) => dispatch(selectpoint(point))
  };
};

MainMap.propTypes = {
    getPointIdFromMainMap: PropTypes.func.isRequired,
    getCanvasParameters: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(MainMap);

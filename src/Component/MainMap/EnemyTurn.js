import React from 'react';
import classes from './EnemyTurn.module.css';
import FrameFromArwes from '../FrameFromArwes/FrameFromArwes';


const initializationGame = {gameMode: 'single', gameType: 'regular', gameLength: 'medium', gameDifficulty: 'good', playerName:'K', userCivilization: 'Federation', opponentCivilization: 'Klingon_empire'};

const turn = 'Federation';

let turnNotification = null;

if (initializationGame.userCivilization === turn){
  turnNotification = false;
}else{
  turnNotification = true;
}


const EnemyTurn = () => {
  return(
    <div>
      {turnNotification && <div className={classes.enemyTurn}>
        <FrameFromArwes additionalClass={classes.enemyTurnFrame}>
          <p className={classes.enemyTurnParagraph}> ENEMY TURN </p>
        </FrameFromArwes >
      </div>}
    </div>
 )
};

export default EnemyTurn;

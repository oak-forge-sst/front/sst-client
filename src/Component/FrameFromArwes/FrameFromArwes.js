import React from 'react';
import PropTypes from 'prop-types';
import { Frame } from 'arwes';
import classes from './FrameFromArwes.module.scss';

const FrameFromArwes = ({ additionalClass, children, scrolled }) => (
  <Frame
    className={additionalClass}
    animate
    noBackground
    corners={3}
    classes={{box: scrolled ? classes.scrolled: null, children: classes.children}}>
      {children}
  </Frame>
);


FrameFromArwes.propTypes = {
  additionalClass: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  scrolled: PropTypes.bool
}

export default React.memo(FrameFromArwes);

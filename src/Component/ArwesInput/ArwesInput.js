import React, { useState } from 'react';
import FrameFromArwes from '../../Component/FrameFromArwes/FrameFromArwes.js';
import classes from "./ArwesInput.module.css";
import {messages} from "../ValidationAndMessage/Messages";
import cx from 'classnames'
import PropTypes from 'prop-types';

const ArwesInput =
 ({type, placeholder, name, required, onChange, valid}) => {
    return (
      <>
        <FrameFromArwes additionalClass={classes.input} >
            <input
            required={required}
            type={type}
            placeholder={placeholder}
            name={name}
            onChange={onChange}
            valid={valid}
            />
        </FrameFromArwes>
          <p className={cx(classes.message, {[classes.valid]: !valid})}>
          {messages[`invalid${type}`]}
          </p>
      </>
    )
}

ArwesInput.propTypes = {
  required: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  placeholder:PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  valid: PropTypes.bool
}

export default ArwesInput;

export const messages = {
  invalidemail: 'Please enter a valid email address (Ex: example@example.com)',

  invalidpassword: 'Your password must be at least 8 characters long and include at least one lowercase letter, one uppercase letter and one number'
}

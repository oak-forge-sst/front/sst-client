import React from 'react';
import {Button} from "arwes";
import classes from "./ButtonToForm.module.css";
import PropTypes from 'prop-types';


const ButtonToForm = ({children, animate, onClick}) => {
    return (
      <Button
      className={classes.button}
      animate={animate}
      onClick={onClick}
      >{children}
      </Button>
    )
}

ButtonToForm.propTypes = {
  onClick:PropTypes.func.isRequired,
  children: PropTypes.any.isRequired
}

export default ButtonToForm;

const initialState = {
    score: 189,
    civilization: "Klingon",
    opponents: 23
}


const statusReducer = (state = initialState, action) => {
    switch(action.type) {
        case "UPDATE_STATUS":
            return {
                ...state,
                score: action.payload  
            }
            default:
            return state; 

    }
}

export default statusReducer;
const initialState = {
 shortRangeScanObjects: [ {"x":9,"y":20,"type":"PLANET"},
                          {"x":10,"y":10,"type":"ENTERPRISE"},
                          {"x":11,"y":10,"type":"SUPER_COMMANDER"},
                          {"x":11,"y":11,"type":"SUPER_COMMANDER"},
                          {"x":12,"y":12,"type":"SUPER_COMMANDER"},
                          {"x":11,"y":13,"type":"KLINGON"},
                          {"x":10,"y":9,"type":"ENTERPRISE"},
                          {"x":9,"y":9,"type":"COMMANDER"},
                          {"x":9,"y":5,"type":"STARBASE"},
                          {"x":9,"y":6,"type":"STARBASE"},
                          {"x":9,"y":7,"type":"STARBASE"},
                          {"x":11,"y":20,"type":"STAR"},
                          {"x":12,"y":20,"type":"STAR"},
                          {"x":13,"y":20,"type":"BLACK_HOLE"},
                          {"x":14,"y":20,"type":"BLACK_HOLE"},
                          {"x":15,"y":20,"type":"STAR"},
                          {"x":18,"y":20,"type":"PLANET"}],
}

const reducer = (state = initialState, action) => {
  return state;
};


export default reducer;


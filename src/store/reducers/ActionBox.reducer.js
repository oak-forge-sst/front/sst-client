import { SELECTPOINT } from '../actions/ActionBox.action'

const initialState = {
  selectedPoint: ''
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SELECTPOINT:
    return{
      selectedPoint: action.payload.selectedPoint
    }
   }
  return state;
};


export default reducer;

export const TOGGLEUNITS = 'TOGGLEUNITS'
export const TOGGLEMAP = 'TOGGLEMAP'

export const toggleunits = (id) => {
  return {
    type: TOGGLEUNITS,
    payload: {
      id: id
    }
  };
};


export const togglemap = (index) => {
  return {
    type: TOGGLEMAP,
    payload: {
      index: index
    }
  };
};

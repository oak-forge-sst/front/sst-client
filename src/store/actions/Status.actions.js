 const updateStatus = (number) => {
    return {
        type: "UPDATE_STATUS",
        payload: number
    }
}

export default updateStatus
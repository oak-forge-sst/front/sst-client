export const SELECTPOINT = 'SELECTPOINT'

export const selectpoint = (index) => {
  return {
    type: SELECTPOINT,
    payload: {
      selectedPoint: index
    }
  };
};
